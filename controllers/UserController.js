const io = require('../io');
const requestJson = require('request-json')
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechu/collections/";
const mLabAPIKey = "apiKey=G16LdGcBQ88ZyjId7l-Kgsa3ENxsOJIG"
const crypt = require('../crypt.js')


function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");


  var result = {};
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
     users.slice(0, req.query.$top) : users;

  res.send(result);
}

function createUserV1(req, res){
  console.log("POST /apitechu/v1/users");
  //devolvemos las cabaceras del cliente
  console.log("first name es " + req.body.first_name);
  console.log("last name es " + req.body.last_name);
  console.log("email  es " + req.body.email);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  };

  var users = require('../usuarios.json');
  users.push(newUser);
  io.writeUserDataToFile(users);


  console.log("Usuario añadido con éxito!");

  res.send({"msg": "Usuario añadido con éxito"});


}



function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id)
  var users = require('../usuarios.json')
  borrado = false;

/*  for (i in users) {
    if (users[i]["id"] == req.params.id){
      console.log('Borrado el usuario con id : ' + users[i]["id"])
      users.splice(i,1)
      borrado = true;
      //writeUserDataToFile(users)
      break;
    }

  }

  // Otro método para buscar
  for (user of users){
     console.log(users.length)
     if (user.id == req.params.id && user!=null){
       console.log("Borrado con for of el usuaro con id ")
       delete users[user.id -1]
       break;

     }
  }*/

/* OTRO MÉTODO
  users.forEach(function (user, index){
    if (user.id = req.params.id){
      console.log("La id coincide")
      users.splice(index,i)
    }
  });

  */

  index = users.findIndex(x => x.id == req.params.id);


  if (index != -1){
    users.splice(index,1)
    console.log("Usuario  id "+ req.params.id + "borrado")
    console.log(users)
    res.send({"msg": "Usuario id " + req.params.id+ " borrado"});
    io.writeUserDataToFile(users);
  }
  else{
    res.send({"msg": "Usuario no encontrado"})
  }


/*    if (borrado == true){
    res.send({"msg": "Usuario id " + req.params.id+ " borrado"});
  }
  else{
    res.send({"msg": "Usuario no encontrado"})
  } */
}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created")
  httpClient.get("user?" + mLabAPIKey,
   function(err, resMLab, body){
     var response = ! err ? body : {
       "msg" : "Error obteniendo usuarios"
     }
     res.send(response);

   }
 );

}


function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");
  var id = req.params.id;
  var query = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created")
  httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body){
     if (err){
       var response = {
         "msg" : "Error obteniendo usuario"
       }
       res.status(500)
     } else {
       if (body.length > 0){
         var response = body[0];
       } else {
         var response = {
           "msg" : "Usuario no encontrado"
         }
         res.status(404)
       }
     }
     // var response = ! err ? body : {
     //   "msg" : "Error obteniendo usuario"
     // }
     res.send(response);

   }
 );

}

function createUserV2(req, res){

  console.log("POST /apitechu/v2/users")
  console.log("id es " + req.body.id);
  console.log("first name es " + req.body.first_name);
  console.log("last name es " + req.body.last_name);
  console.log("email  es " + req.body.email);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  }

  console.log("Client created")
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.post("user?"  + mLabAPIKey, newUser,
   function(err, resMLab, body){
     console.log("Usuario creado con éxito")
     res.send({"msg": "Usuari creado con éxito"})
   }
  )

}










module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
