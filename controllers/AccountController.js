const io = require('../io');
const requestJson = require('request-json')
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechu/collections/";
const mLabAPIKey = "apiKey=G16LdGcBQ88ZyjId7l-Kgsa3ENxsOJIG"
const crypt = require('../crypt.js')





function getAccountById(req, res) {
  console.log("GET /apitechu/v2/users/:id/accounts/");
  console.log(req.params)
  var id = req.params.id;
  var query = 'q={"userid":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("cuenta?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body){
     if (err){
       var response = {
         "msg" : "Error obteniendo cuentas"
       }
       res.status(500)
     } else {
       if (body.length > 0){
         var response = body;
       } else {
         var response = {
           "msg" : "Usario sin cuentas"
         }
         res.status(404)
       }
     }

     res.send(response);

   }
 );

}
module.exports.getAccountById = getAccountById;
