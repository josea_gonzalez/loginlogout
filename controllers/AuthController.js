const io = require('../io');
const requestJson = require('request-json')
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechu/collections/";
const mLabAPIKey = "apiKey=G16LdGcBQ88ZyjId7l-Kgsa3ENxsOJIG"
const crypt = require('../crypt')


function loginV1(req,res){
  console.log("POST /apitechu/v1/login")
  console.log(req.body.email)

  var users = require('../usuarios.json')
  //res.send(users)
  resultado = {"mensaje" : "Login incorrecto"}

  for (user of users){
    //console.log("dentro")
    if ( (user.email == req.body.email) && (user!=null)
        && (user.password == req.body.password)
        && (!user.logged)){
      console.log("Login correcto")
      user.logged = true
      resultado = {"Mensaje": "Login Correcto",
                    "idUsuario": user.id};
      io.writeUserDataToFile(users);
      break;
    }

  }

  console.log(resultado)


  res.send(resultado)

}


function logoutV1(req,res){
  console.log("POST /apitechu/v1/logout/")
  console.log(req.body.id)

  var users = require('../usuarios.json')
  resultado = {"mensaje" : "Logout incorrecto"}

  for (user of users){
    //console.log("dentro")
    if ( (user.id == req.body.id) && (user!=null) && (user.logged) ){
      console.log("Logout correcto")

      resultado = {"Mensaje" : "Log out correcto ",
                  "idUsuario" : user.id};

      delete user.logged
      io.writeUserDataToFile(users);
      break;
    }

  }



  res.send(resultado)


}








// function loginV2(req,res){
//   console.log("POST /apitechu/v2/login/ ARRRRR")
//   console.log(req.body)
//   console.log(req.body.email)
//   var email = req.body.email
//   var password = req.body.password
//
//   var query = 'q={"email": "' + email + '"}'
//
//   console.log("Conexión establecida con query:" + query)
//
//   var httpClient = requestJson.createClient(baseMLabURL);
//
//   httpClient.get("user?" + query + "&" + mLabAPIKey,
//    function(err, resMLab, body){
//      var isPasswordcorrect = crypt.checkPassword(password, body[0].password)
//
//
//      if (err){
//        var response = {
//          "msg" : "Error obteniendo usuario por email"
//        }
//        res.status(500)
//      } else {
//        if ( (body.length > 0) && (crypt.checkPassword(password,body[0].password))){
//          var response = body[0];
//          console.log("Encontrado el usuario por email")
//          var putBody='{"$set":{"logged":true}}'
//
//          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
//            function(errPUT, resMLabPUT, bodyPUT){
//              console.log("Usuario logado con éxito")
//              res.send({"msg": "Usuario logado con éxito"})
//            }
//         );
//
//        } else {
//          var response = {
//            "msg" : "Usuario no encontrado por email"
//          }
//          res.send({"msg": "Error login!"})
//          res.status(404)
//
//        }
//      }
//
//      //res.send(response);
//
//    }
//  );
// }
function loginV2(req, res) {
 console.log("POST /apitechu/v2/login");
 var email = req.body.email;
 var password = req.body.password;

 var query = 'q={"email": "' + email + '"}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     console.log(body)
     var isPasswordcorrect =
       crypt.checkPassword(password, body[0].password);
     console.log("Password correct is " + isPasswordcorrect);

     if (body.length == 0 || !isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}


function logoutV2(req,res){
  console.log("POST /apitechu/v2/logout/ ARRRRR")
  console.log(req.body.email)
  var email = req.body.email
  var query = 'q={"email": "' + email + '"}'
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Conexión establecida ")
  httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body){
     if (err){
       var response = {
         "msg" : "Error obteniendo usuario por email"
       }
       res.status(500)
     } else {
       if (body.length > 0){
         var response = body[0];
         console.log("Deslogado el usuario por email")

         var putBody='{"$unset":{"logged":""}}'
         console.log(putBody)
         console.log(JSON.parse(putBody))

         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(err, resMLab, body){
             console.log("Usuario deslogado con éxito")
             res.send({"msg": "Usuario deslogado con éxito"})
           }
        );



       } else {
         var response = {
           "msg" : "Usuario no encontrado por email"
         }
         res.send({"msg": "Error no unlogin!"})
         res.status(404)
       }
     }



   }
 );
}







module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
