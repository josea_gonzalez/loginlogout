
const fs = require('fs');

function writeUserDataToFile(data){
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./usuarios.json",jsonUserData, "utf8",
  //funcion para manejar los errores
    function(err){
      if (err) {
        console.log(err)
      } else {
        console.log("Datos escritos en fichero.");
      }
    }
  )
}

module.exports.writeUserDataToFile = writeUserDataToFile;
