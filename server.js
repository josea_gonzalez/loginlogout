// Cargamos la libreria express
const express = require('express');
const app = express(); //Cargamos las funciones del framework
const port = process.env.PORT || 3000;

// Parseo de JSON
const bodyParser =require('body-parser');
app.use(bodyParser.json())
// Parseo de JSON
app.listen(port);
console.log("HOLA 20180625!" + port);


const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
//Vamos a usar el framework para crear nuestra primera ruta
//Para la ruta "" definimos un get
app.get('/apitechu/v1/hello',
//Parámetros que crea el framework
//req: peticion recibida
//res: respuesta que mandamos
//Función manejadora
  function(req, res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg": "API TechU + OpenShift!!!"});
  }
);

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req,res){
    console.log("POST /apitechu/v1/users/:p1/p2")

    console.log("Parametros")
    console.log(req.params);

    console.log("Query String")
    console.log(req.query)

    console.log("Headers")
    console.log(req.headers)

    console.log("body")
    console.log(req.body)
  }
);



  app.get("/apitechu/v1/users",userController.getUsersV1);
  app.post('/apitechu/v1/users', userController.createUserV1);
  app.post('/apitechu/v2/users', userController.createUserV2);
  app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);
  app.get("/apitechu/v2/users", userController.getUsersV2);
  app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);
  app.post('/apitechu/v1/login',authController.loginV1);
  app.post('/apitechu/v1/logout/', authController.logoutV1);


  app.post('/apitechu/v2/login/',authController.loginV2);
  app.post('/apitechu/v2/logout/', authController.logoutV2);


  app.get("/apitechu/v2/users/:id/accounts/", accountController.getAccountById);
